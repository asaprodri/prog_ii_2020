import time
import torch


# We will build nn to replace this model


def model(t_u, w, b):
    """The model to predict temperature"""
    return w * t_u + b


# Pytorch comes with a number of predifined loss functions you can just import
# For example: https://pytorch.org/docs/stable/generated/torch.nn.MSELoss.html
# But MSE is basically what you see below

def loss_fn(t_p, t_c):
    """The loss function used. Less loss is what we want"""
    squared_diffs = (t_p - t_c) ** 2
    return squared_diffs.mean()

# Before learning about backprop, we are going to do things manually. Concretely
# we are going to calculate the partial derivatives analytically


def dloss_fn(t_p, t_c):
    """"""
    dsq_diffs = 2 * (t_p - t_c) / t_p.size(0)  # <1>
    return dsq_diffs


def dmodel_dw(t_u, w, b):
    return t_u


def dmodel_db(t_u, w, b):
    return 1.0

# Using the partial derivatives and the chain rule, we can compute a gradient, again
# analytically. We will never do this again. We will use backprop algorithm, using pytorch.


def grad_fn(t_u, t_c, t_p, w, b):
    dloss_dtp = dloss_fn(t_p, t_c)
    dloss_dw = dloss_dtp * dmodel_dw(t_u, w, b)
    dloss_db = dloss_dtp * dmodel_db(t_u, w, b)
    return torch.stack([dloss_dw.sum(), dloss_db.sum()])  # <1>


# Now the training loop, which contains the main steps. Once we learn to do this in pytorch
# we will:
# 1. Use pytorch modules to build network architectures
# 2. Use backprop to calculate partial derivatives
# 3. Use built in loss functions
# 4. Use built in optimizers (gradient descent but also others).


def training_loop(n_epochs, learning_rate, params, t_u, t_c,
                  print_params=True):
    for epoch in range(1, n_epochs + 1):
        w, b = params

        t_p = model(t_u, w, b)  # <1>
        loss = loss_fn(t_p, t_c)
        grad = grad_fn(t_u, t_c, t_p, w, b)  # <2>

        params = params - learning_rate * grad

        if epoch in {1, 2, 3, 10, 11, 99, 100, 4000, 5000}:  # <3>
            print('Epoch %d, Loss %f' % (epoch, float(loss)))
            if print_params:
                print('    Params:', params)
                print('    Grad:  ', grad)
        if epoch in {4, 12, 101}:
            print('...')

        if not torch.isfinite(loss).all():
            break  # <3>

    return params


def main():
    t_c = [0.5, 14.0, 15.0, 28.0, 11.0, 8.0, 3.0, -4.0, 6.0, 13.0, 21.0]
    t_u = [35.7, 55.9, 58.2, 81.9, 56.3, 48.9, 33.9, 21.8, 48.4, 60.4, 68.4]
    t_c = torch.tensor(t_c)
    t_u = torch.tensor(t_u)

    params = training_loop(
        n_epochs=5000,
        learning_rate=1e-4,
        params=torch.tensor([1.0, 0.0]),
        t_u=t_u,
        t_c=t_c)

    t_p = model(t_u, *params)
    print(t_p)
    print(t_c)


if __name__ == '__main__':
    torch.set_printoptions(edgeitems=2, linewidth=75)
    start = time.time()
    main()
    print(f"Ellapsed time: {time.time() - start} s")
