from dataclasses import dataclass

class User:
    def __init__(self, name: str):
        self.name = name
        self.__email = None

    def add_email(self, email: str):
        if isinstance(email, str) and '@' in email:
            self._email = email

    def print_email(self):
        return f'{self.name} <{self._email}>'


u1 = User('Mary')
u1.add_email('m@ufv.es')
u1.print_email()