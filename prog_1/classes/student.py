# Adapted from https://medium.com/better-programming/advanced-python-9-best-practices-to-apply-when-you-define-classes-871a27af658b

class Student:
    def __init__(self,
                 first_name: str,
                 last_name: str,
                 grades: list[float]):

        self.first_name = first_name
        self.last_name = last_name
        self.grades = grades

    def mean(self):
        return sum(self.grades)/len(self.grades)

    def __repr__(self):
        return f"Student({self.first_name!r}, {self.last_name!r})"

    def __str__(self):
        return f"Student: {self.first_name} {self.last_name}"


student1 = Student("David", "Peterson", [4.0, 7.0])
student2 = Student("John", "Doe", [5.5, 8.9, 10.0])

d = {
    student1.last_name: student1,
    student2.last_name: student2
}

student1.mean()

sum(student1.grades)/len(student1.grades)

sum(d["Peterson"].grades)/len(d["Peterson"].grades)

d["Peterson"].mean()

student_ids = ['001', '002', '004']
names = ['John', 'Peter', 'Mary']
surnames = ['Johnson', 'Doe', 'Peterson']

students = {}
for student_id, name, surname in zip(student_ids, names, surnames):
    students[student_id] = Student(name, surname)

students['004'].first_name = 'Mary Joe'
