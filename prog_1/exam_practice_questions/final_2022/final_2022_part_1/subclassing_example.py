class Container:

    def __init__(self, width: float, length: float, height: float):
        self._width = width
        self._length = length
        self._height = height
        self._items = []

    def capacity(self) -> float:
        return self._width * self._length * self._height

    def add_item(self, item: str):
        self._items.append(item)


class MovingBox(Container):
    def __init__(self, width: float, length: float):
        super().__init__(width, length, 100)


class WoodCrate(Container):
    def __init__(self, width: float, length: float):
        super().__init__(width, length, 200)

    def capacity(self):
        return super().capacity() * 0.8


# derived class


obj_1 = MovingBox(50, 60)
obj_1.capacity()
# 300000.0
obj_2 = WoodCrate(50, 80)
obj_2.capacity()
# 640000.0
