import datetime as dt
from collections import defaultdict

s = [
    ('accupril', 1, dt.date(2022, 4, 3)),
    ('skelaxin', 3, dt.date(2022, 4, 1)),
    ('lipitor', 5, dt.date(2022, 4, 5)),
    ('genotropin', 6, dt.date(2022, 4, 10)),
    ('genotropin', 8, dt.date(2022, 4, 8)),
    ('accupril', 2, dt.date(2022, 4, 9)),
    ('skelaxin', 10, dt.date(2022, 4, 3)),
    ('lipitor', 7, dt.date(2022, 4, 1)),
    ('skelaxin', 1, dt.date(2022, 4, 2))
]


# q1 cantidades vendidas de cada producto

def q_sold(records: list[tuple[str, int, dt.date]]) -> defaultdict[str, int]:
    totals = defaultdict(int)
    for sku, q, date in records:
        totals[sku] += q
    return totals


print(q_sold(s))
# defaultdict(<class 'int'>, {'accupril': 3, 'skelaxin': 14, 'lipitor': 12, 'genotropin': 14})

# q2 conjunto de productos vendidos a partir de una fecha


def products_from_date(records: list[tuple[str, int, dt.date]],
                  d: dt.date) -> set[str]:
    products = {sku for sku, q, date in records if date >= d}
    return products


print(products_from_date(s, dt.date(2022, 4, 9)))
# {'accupril', 'genotropin'}


# q3, productos en un día

def daily_products(records: list[tuple[str, int, dt.date]]) -> defaultdict[dt.date, set[str]]:
    daily_totals = defaultdict(set)
    for sku, q, date in records:
        daily_totals[date].add(sku)
    return daily_totals


dp = daily_products(s)
print(dp)
# defaultdict(<class 'set'>, {
# datetime.date(2022, 4, 3): {'accupril', 'skelaxin'},
# datetime.date(2022, 4, 1): {'skelaxin', 'lipitor'},
# datetime.date(2022, 4, 5): {'lipitor'},
# datetime.date(2022, 4, 10): {'genotropin'},
# datetime.date(2022, 4, 8): {'genotropin'},
# datetime.date(2022, 4, 9): {'accupril'},
# datetime.date(2022, 4, 2): {'skelaxin'}})
print(dp[dt.date(2022, 4, 3)])
# {'accupril', 'skelaxin'}

# q4 función que devuelve si un producto se ha vendido


def check(records: list[tuple[str, int, dt.date]], product: str) -> bool:
    product_set = {sku for sku, q, date in records}
    return product in product_set


check(s, 'accupril')
# True
check(s, 'sinequan')
# False