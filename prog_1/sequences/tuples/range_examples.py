r = range(0, 20, 2)
"""
>>> r
>>> 11 in r
>>> 10 in r
"""

r.index(10)

"""
>>> r[5]
>>> r[:5]
>>> r[-1]
"""

def f(x: tuple[int, int]):
    return sum(x)


f(2,3)

