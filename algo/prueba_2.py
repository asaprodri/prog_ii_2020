import logging

logger = logging.getLogger()
logging.basicConfig(level=logging.DEBUG)

handler = logging.FileHandler('mylog.log')
logger.addHandler(handler)

formatter = logging.Formatter('%(asctime)s- %(module)s [%(levelname)s] %(funcName)s: %(message)s',
                              datefmt='%d/%m/%Y %I:%M:%S %p')
handler.setFormatter(formatter)


def f(x: int) -> int:
    """Sqares"""
    logger.info(f'Calling f with value {x}')
    return x ** 2


logger.debug('Calling main')
print(f(2))
