import logging.config
import yaml
from email_validator import validate_email, EmailNotValidError

with open('logging_config_2.yaml', 'r') as f:
    config = yaml.safe_load(f.read())
    logging.config.dictConfig(config)

logger = logging.getLogger('myLogger')

email = "my+address@mydomain.tld"

logger.debug(f"Checking email: {email}")
try:
    # Validate.
    valid = validate_email(email)
    logger.info(f"Validated email: {valid.email}")
    # Update with the normalized form.
    email = valid.email
except EmailNotValidError as e:
    # email is not valid, exception message is human-readable
    logger.warning(f"Bad email: {email}, error {e}")
