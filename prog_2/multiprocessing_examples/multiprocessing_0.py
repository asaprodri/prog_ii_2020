import time

start = time.perf_counter()


def do_something(seconds):
    print(f'Sleeping {seconds} second(s)...')
    time.sleep(seconds)
    print(f'Done Sleeping...{seconds}')


if __name__ == '__main__':
    do_something(1)
    do_something(1)

    finish = time.perf_counter()

    print(f'Finished in {round(finish-start, 2)} second(s)')

# Adapted from Youtube coures: multiprocessing https://www.youtube.com/watch?v=fKl2JW_qrso
# https://github.com/CoreyMSchafer/code_snippets/tree/master/Python/MultiProcessing