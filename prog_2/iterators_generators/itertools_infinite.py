from itertools import cycle

j = 0
for i in cycle('ABCDE'):
    print(i)
    if i == 'E':
        j += 1
    if j >= 3:
        break
