import unittest
from prog_2.unittest_calc.test_calculator import TestCalculator


def t_suite():
    suite = unittest.TestSuite()
    suite.addTest(TestCalculator('test_initial_value'))
    suite.addTest(TestCalculator('test_add_one'))
    return suite


if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    runner.run(t_suite())