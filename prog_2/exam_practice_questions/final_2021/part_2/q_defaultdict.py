from collections import defaultdict

d = defaultdict(dict)

articles = ["apple", "banana", "apple", "cherry", "strawberry", "apple", "raspberry", "almond", "avocado", "barberry"]

for i, item in enumerate(articles):
    d[item[0]].update({i: item})


"""
En la primera iteración, la llamada a d['a'] devuelve un diccionario vacío
En la primera iteración, el diccionario asociado a d['a'] se actualiza con {0: 'apple') 
F En la tercera iteración (segunda aparición de 'appple' en la lista), se devuelve un segundo diccionario vacío
Al final de la ejecución del bucle for, d tendrá 5 entradas
Al final de la ejecución del bucle for, d['a'] será un diccionario con 5 entradas
"""