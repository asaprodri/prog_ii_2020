from collections import ChainMap

baseline = {'music': 'bach', 'art': 'rembrandt'}
adjustments = {'art': 'van gogh', 'opera': 'carmen'}

cm = ChainMap(adjustments, baseline)

print(cm['music'])
print(cm['art'])

cm2 = cm.new_child(m={'art': 'picasso', 'opera': 'la_traviata'})

# Returns a new ChainMap containing a new map m followed by all of the maps in cm,
# that is, adjustments and baseline

print(cm2['art'])

# This returns piscasso, as it is prepended
