from dataclasses import dataclass
import heapq

l = [21, 1, 45, 78, 3, 5]
# Covert to a heap
heapq.heapify(l)  # O(n)

# Add element
heapq.heappush(l, 8)  # O(log(n))

while True:
    try:
        print(heapq.heappop(l))  # O(1)
        print(l)
    except IndexError:
        print('End')
        break


@dataclass
class Job:
    id: int
    name: str


taks = [
    (10, Job(1, 'study ai')),
    (4, Job(2, 'study ml')),
    (1, Job(3, 'study dss'))]

# Covert to a heap
heapq.heapify(taks)  # O(n)

# Add element
heapq.heappush(taks, (0, Job(3, 'study prog II')))  # O(log(n))

while True:
    try:
        task = heapq.heappop(taks)
        print(task[1])  # O(1)
    except IndexError:
        print('End')
        break
